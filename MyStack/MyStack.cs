﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MyStack
{
    public class Stack<T>: IEnumerable<T>
    {
        private LinkedList<T> list;

        private int maxSize;
        
        public int MaxSize
        {
            get { return maxSize; }
        }
        
        public int Count
        {
            get { return list.Length; }
        }
        
        public bool IsEmpty
        {
            get { return list.IsEmpty; }
        }
        
        public event EventHandler<ElementAddedEventArgs<T>> ElementAdded;
        
        public event EventHandler<ElementRemovedEventArgs<T>> ElementRemoved;

        public Stack(int size = Int32.MaxValue)
        {
            list = new LinkedList<T>();
            maxSize = size;
        }

        public void Push(T data)
        {
            if (list.Length == maxSize)
            {
                throw new InvalidOperationException("Stack overflow!");
            }

            list.PushFront(data);

            ElementAddedEventArgs<T> args = new(data);
            ElementAdded?.Invoke(this, args);
        }

        public void PushWithResize(T data)
        {
            if (list.Length == maxSize)
            {
                maxSize *= 2;
            }
            
            list.PushFront(data);
            
            ElementAddedEventArgs<T> args = new(data);
            ElementAdded?.Invoke(this, args);
        }

        public T Pop()
        {
            if (list.Length == 0)
            {
                throw new InvalidOperationException("Stack is empty!");
            }

            T removedElement = list.RemoveFront().Data;
            
            ElementRemovedEventArgs<T> args = new(removedElement);
            ElementRemoved?.Invoke(this, args);
            
            return removedElement;
        }

        public bool TryPop(out T result)
        {
            if (list.Length == 0)
            {
                result = default;
                return false;
            }

            result = list.RemoveFront().Data;
            return true;
        }

        public T Peek()
        {
            if (list.Length == 0)
            {
                throw new InvalidOperationException("Stack is empty!");
            }

            return list.PeekFront().Data;
        }

        public bool TryPeek(out T result)
        {
            if (list.Length == 0)
            {
                result = default;
                return false;
            }

            result = list.PeekFront().Data;
            return true;
        }

        public void Clear()
        {
            list.Clear();
        }
        
        public bool Contains(T data)
        {
            return list.Contains(data);
        }

        public void CopyTo(Stack<T> resultStack)
        {
            list.CopyTo(resultStack.list);
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
    }
}