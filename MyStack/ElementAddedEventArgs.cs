﻿using System;

namespace MyStack
{
    public class ElementAddedEventArgs<T>: EventArgs
    {
        public T element { get; }

        public ElementAddedEventArgs(T data)
        {
            element = data;
        }
    }
}