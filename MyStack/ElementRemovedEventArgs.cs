﻿using System;

namespace MyStack
{
    public class ElementRemovedEventArgs<T>: EventArgs
    {
        public T element { get; }
        
        public ElementRemovedEventArgs(T data)
        {
            element = data;
        }
    }
}