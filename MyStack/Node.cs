﻿namespace MyStack
{
    public class Node<TValue>
    {
        public TValue Data { get; set; }
        public Node<TValue> Next { get; set; }
        
        public Node(TValue data) => Data = data;

        public Node(TValue data, Node<TValue> next)
        {
            Data = data;
            Next = next;
        }
    }
}