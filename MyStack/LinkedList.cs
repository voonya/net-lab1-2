﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyStack;

namespace MyStack
{
    public class LinkedList<T>: IEnumerable<T>
    {
        private Node<T> head;
        private Node<T> tail;
        private int length = 0;

       
        public int Length
        {
            get { return length; }
        }
        
        public bool IsEmpty
        {
            get { return Length == 0; }
        }

        public void PushBack(T data)
        {
            Node<T> newNode = new(data);

            if (head == null)
            {
                head = newNode;
            }
            else
            {
                tail.Next = newNode;
            }

            tail = newNode;

            length++;
        }

        public void PushFront(T data)
        {
            Node<T> newNode = new(data);
            if (head == null)
            {
                tail = newNode;
            }
            else
            {
                newNode.Next = head;
            }
            head = newNode;

            length++;
        }

        public Node<T> RemoveFront()
        {
            if (IsEmpty)
            {
                throw new InvalidOperationException("The list are empty!");
            }

            Node<T> nodeToRemove = head;

            head = head.Next;

            if (head == null)
            {
                tail = null;
            }

            length--;
            
            return nodeToRemove;
        }

        public Node<T> PeekFront()
        {
            return head;
        }

        public void Clear()
        {
            length = 0;
            head = null;
            tail = null;
        }

        public bool Contains(T data)
        {
            Node<T> current = head;
            while (current != null)
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            return false;
        }

        public void CopyTo(LinkedList<T> resultList)
        {
            Node<T> current = head;
            while (current != null)
            {
                
                resultList.PushBack(current.Data);
                current = current.Next;
            }
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
    }
}