using System;
using Xunit;
using MyStack;
    
namespace MyStack.Tests
{
    public class UnitTests
    {
        [Fact]
        public void IsEmpty_PushElementAndCheckIfStackEmpty_ReturnFalse()
        {
            Stack<string> stack = new();

            string element = "Hello World";
            
            
            stack.Push(element);

            Assert.False(stack.IsEmpty, "Should equal false, because stack is not empty");
        }
        
        [Fact]
        public void Push_AddElementHelloWorld_ContainHelloWorldAndLengthEqual1()
        {
            Stack<string> stack = new();

            string element = "Hello World";

            int count = 1;
            
            stack.Push(element);

            Assert.True(stack.Contains(element));

            Assert.Equal(stack.Count, count);
        }
        
        [Fact]
        public void Push_AddElementWhenStackIsFull_ThrowInvalidOperationException()
        {
            Stack<string> stack = new(1);

            string element = "Hello World";
            
            stack.Push(element);

            Assert.Throws<InvalidOperationException>(() => stack.Push(element));
        }
        
        [Fact]
        public void PushWithResize_PushWithResizeElement_MaxSizeEqual2()
        {
            Stack<string> stack = new(1);

            string element = "Hello World";
            
            stack.Push(element);

            stack.PushWithResize(element);

            int maxSizeExpected = 2;

            Assert.Equal(stack.MaxSize, maxSizeExpected);
        }

        [Fact] 
        public void Pop_PopElement_ReturnElementHelloWorld()
        {
            Stack<string> stack = new();

            string element = "Hello World";
            
            stack.Push(element);

            string popedElement = stack.Pop();

            Assert.Equal(popedElement, element);
        }
        
        [Fact]
        public void Pop_PopElementWhenStackEmpty_ThrowInvalidOperationException()
        {
            Stack<string> stack = new();
            
            Assert.Throws<InvalidOperationException>(() => stack.Pop());
        }
        
        [Fact]
        public void TryPop_TryPopNoEmptyStack_ReturnTrueAndElementHelloWorld()
        {
            Stack<string> stack = new();
            
            string element = "Hello World";
            
            stack.Push(element);

            string popedElement;
            
            Assert.True(stack.TryPop(out popedElement), "Should return true, because stack is not empty");
            
            Assert.Equal(popedElement, element);
        }

        [Fact]
        public void Peek_PeekElementHelloWorld_CountOfElementsEqual2()
        {
            Stack<string> stack = new();
            
            stack.Push("smth");
            
            string element = "Hello World";
            
            stack.Push(element);

            string peekedElement = stack.Peek();
            
            Assert.Equal(peekedElement, element);
            
            Assert.Equal(stack.Count, 2);
        }
        
        [Fact]
        public void Peek_PeekElementWhenStackEmpty_ThrowInvalidOperationException()
        {
            Stack<string> stack = new();
            
            Assert.Throws<InvalidOperationException>(() => stack.Peek());
        }
        
        
        [Fact]
        public void TryPeek_TryPeekNoEmptyStack_ReturnTrueAndElementHelloWorld()
        {
            Stack<string> stack = new();
            
            string element = "Hello World";
            
            stack.Push(element);

            string popedElement;
            
            Assert.True(stack.TryPeek(out popedElement), "Should return true, because stack is not empty");
            
            Assert.Equal(popedElement, element);
        }
        
        
        [Fact]
        public void Clear_ClearNoEmptyStack_ReturnCount0AndTryPeekReturnFalse()
        {
            Stack<string> stack = new();
            
            string element = "Hello World";
            
            stack.Push(element);

            stack.Clear();

            string popedElement;
            
            Assert.Equal(stack.Count, 0);
            
            Assert.False(stack.TryPeek(out popedElement), "Should return false, because stack is empty");
        }

        [Fact]
        public void Enumerator_ForEachStack_EnumeratorReturnsValuesThatStackContains()
        {
            string[] elements = { "1", "2", "3", "4" };
            int index = elements.Length - 1;
            
            Stack<string> stack = new();
            foreach (string element in elements)
            {
                stack.Push(element);
            }

            foreach (string element in stack)
            {
                Assert.Equal(elements[index], element);
                index--;
            }
        }
        
        [Fact]
        public void CopyTo_CopyStackToAnother_ElementsOfFirstStackEqualElementsOfSecondStack()
        {
            string[] elements = { "1", "2", "3", "4" };

            Stack<string> stack = new();
            foreach (string element in elements)
            {
                stack.Push(element);
            }

            Stack<string> copyStack = new();
            stack.CopyTo(copyStack);
            
            Assert.Equal(copyStack, stack);
        }
        
        [Fact]
        public void Push_Push1_ElementAddedEventRaised()
        {
            Stack<int> stack = new();
                
            int expectedEventData = 1;

            var receivedEvent = Assert.Raises<ElementAddedEventArgs<int>>(
                a => stack.ElementAdded += a,
                a => stack.ElementAdded -= a,
                () => stack.Push(1)
            );
        
            Assert.NotNull(receivedEvent);
            Assert.Equal(expectedEventData, receivedEvent.Arguments.element);
        }
        
        [Fact]
        public void Pop_PopElement_ElementRemovedEventRaised()
        {
            Stack<int> stack = new();
                
            int data = 1;
            
            stack.Push(data);

            var receivedEvent = Assert.Raises<ElementRemovedEventArgs<int>>(
                a => stack.ElementRemoved += a,
                a => stack.ElementRemoved -= a,
                () => stack.Pop()
            );
        
            Assert.NotNull(receivedEvent);
            Assert.Equal(data, receivedEvent.Arguments.element);
        }
    }
}