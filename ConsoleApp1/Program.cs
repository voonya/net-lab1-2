﻿using System;
using MyStack;

namespace ConsoleApp1
{
    class Program
    {
        public struct User
        {
            public User(string n, string sn)
            {
                name = n;
                surname = sn;
            }
            public string name;
            public string surname;

            
        }
        static void Main(string[] args)
        {
            Stack<string> myStack = new Stack<string>();
            myStack.ElementAdded += (_, args) => Console.WriteLine($"Event: {args.element}");
            
            string? result;
            Console.WriteLine($"Try pop empty: {myStack.TryPop(out result)}");
            
            myStack.Push("First word");
            Console.WriteLine($"Peek: {myStack.Peek()}");

            myStack.Push("Second");
            myStack.Push("Third");
            myStack.Push("Fourth");
            myStack.Push("Fifth");
            
            Console.WriteLine($"Contains 'Fourth': {myStack.Contains("Fourth")}");
            Console.WriteLine($"Contains 'Six': {myStack.Contains("Six")}");
            
            
            Console.WriteLine("\n-------------\n");
            Stack<User> newStack = new Stack<User>();
            newStack.ElementAdded += (_, args) => Console.WriteLine($"Event add: {args.element.name}, {args.element.surname}");
            newStack.ElementRemoved += (_, args) => Console.WriteLine($"Event remove: {args.element.name}, {args.element.surname}");
            
            newStack.Push(new User("Ivan", "nikolaiev"));
            newStack.Push(new User("Petro", "Pupkin"));
            newStack.Push(new User("Serhii", "Vasyzn"));
            
            foreach (var user in newStack)
            {
                Console.WriteLine(user.name, user.surname);
            }

            Stack<User> copyStack = new Stack<User>();
            newStack.CopyTo(copyStack);

            Console.WriteLine(newStack.Contains(new User("Ivan", "Vasyzn")));
            newStack.Pop();
            newStack.Pop();
            newStack.Pop();

            try
            {
                Console.WriteLine(newStack.Pop());
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Invalid operation!");
            }

            foreach (var user in copyStack)
            {
                Console.WriteLine($"{user.name}, {user.surname}");
            }
            
            copyStack.Clear();
            
            Console.WriteLine($"Length: {copyStack.Count}");
            Console.WriteLine($"Empty?: {copyStack.IsEmpty}");
        }
    }
}